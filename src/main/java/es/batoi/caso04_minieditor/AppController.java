package es.batoi.caso04_minieditor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.swing.*;

/**
 * FXML Controller class
 *
 * @author sergio
 */
public class AppController implements Initializable {

	@FXML
	private TextArea taEditor;
	@FXML
	private TextField tfURL;
	@FXML
	private Label lblInfo;
	@FXML
	private Button btnAbrir;
	@FXML
	private Button btnCerrar;
	@FXML
	private Button btnGuardar;
	@FXML
	private Button btnNuevo;

	private Stage escenario;
	private File f;

	private String textAreaOld ="";

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		taEditor.textProperty()
				.addListener((ObservableValue<? extends String> obs, String oldValor, String newValor) -> {
					if (!oldValor.equals(newValor)) {						
						int pos = 0;
						int numLin = 1;
						while (pos > -1) {
							pos = newValor.indexOf(System.getProperty("line.separator"), pos);
							if (pos > -1) {
								pos++;
								numLin++;
							}
						}
						muestraInfo(numLin, newValor.length() - numLin + 1);
					}
				});

	}

	private void muestraInfo(int numLin, int numCaracteres) {
		lblInfo.setText(numLin + " lineas - " + numCaracteres + " caracteres");
	}

	@FXML
	private void handleNuevo() {
		taEditor.setDisable(false);
		handleGuardar();
		taEditor.setText("");
		System.out.println("Has pulsado botón nuevo");
		f = null;
		textAreaOld = "";
	}

	@FXML
	private void handleAbrir() throws RuntimeException{
		handleGuardar();
		String lin;
		taEditor.setText("");
		taEditor.setDisable(false);
		FileChooser fc = new FileChooser();
		fc.setTitle("ABRIR ARCHIVO");


		Preferences pf = Preferences.userNodeForPackage(AppController.class);

		if(pf.get("ruta1",null) == null   ){
			System.out.println("ruta cambiada");
			f = fc.showOpenDialog(escenario);
			if (f == null) {
				System.out.println("no se seleciono nada");
				return;
			}
			pf.put("ruta1",f.getParent());
			System.out.println("ruta a guardar:->"  +  pf.get("ruta1","/"));
		} else {
			fc.setInitialDirectory(new File(pf.get("ruta1","/")));
			f = fc.showOpenDialog(escenario);
			if (f == null) {
				System.out.println("no se seleciono nada");
				return;
			}
			pf.put("ruta1",f.getParent());
		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			while (  (lin = br.readLine()) !=null){
				taEditor.appendText(lin + System.getProperty("line.separator"));
			}
		}catch (IOException e){
			System.out.println(e.getMessage());
		}
		textAreaOld = taEditor.getText();
		System.out.println("Archivo seleccionado para abrir: " + f.getAbsolutePath());
		//taEditor.setText("Esto es lo que estoy leyendo del fichero");
	}
	@FXML
	private  void   handleGuardar() {
		if ((taEditor.getText().equals(textAreaOld) || taEditor.getText().isEmpty()) /* && f == null*/ ){
			System.out.println("no guardo");
			return ;
		}
		FileChooser fc = new FileChooser();
		System.out.println((taEditor.getText().equals(textAreaOld) || taEditor.getText().isEmpty()));

		fc.setTitle("GUARDAR ARCHIVO");

		if (f == null){
			f = fc.showSaveDialog(escenario);
			if (f == null){
				return ;
			}
		}

		System.out.println(fc);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(f,false));
			bw.write(taEditor.getText());
			bw.close();
		}catch (IOException e){
			System.out.println(e.getMessage());
		}
		System.out.println("Archivo seleccionado para guardar: " + f.getAbsolutePath() + "-- /" );
		textAreaOld = taEditor.getText();

	}

	@FXML
	private void handleCerrar() {
		System.out.println("Has pulsado en cerrar");
		handleGuardar();
		taEditor.setText("");
		taEditor.setDisable(true);
		f = null;
	}


	@FXML
	private void handleLeer() {
		handleGuardar();
		taEditor.setDisable(false);
		String lin;
		taEditor.setText("");
		f = null;

		try {
			URL url = new URL(tfURL.getText());

			try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
				while ((lin = br.readLine()) != null) {
					taEditor.setText(taEditor.getText()+lin+System.getProperty("line.separator"));
				}
			} catch (IOException ex) {
				taEditor.setText("Error de lectura en origen");
			}
		} catch (MalformedURLException ex) {
			taEditor.setText("URL incorrecta");
		}
	}

	void setEscenario(Stage escenario) {
		this.escenario = escenario;
	}




}
